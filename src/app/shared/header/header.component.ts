import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  username: string = '';
  userLogin: any;
  constructor(
    private _authServ: AuthService,
    private _router: Router
  ) { }

  ngOnInit() {
    if (localStorage.getItem('DATA')) {
      this.userLogin = JSON.parse(localStorage.getItem('DATA'));
    }
  }

  logout() {
    this._authServ.logout();
  }

}
