import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/products.model';

//Import Services
import { ProductsService } from 'src/app/services/products.service';
import { Observable } from 'rxjs';
import { ModalService } from './product-detail/productModal.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  products: Product = new Product();
  productSelected: Product;

  constructor(
    private _productServ: ProductsService,
    private _modalProdServ: ModalService
  ) { }

  ngOnInit() {
    this.getProducts();
  }

  public getProducts() {
    this._productServ.getProducts()
      .subscribe((resp: any) => {
        this.products = resp.data;
        console.log(resp.data)
      });
  }

  openModal(product: Product) {
    this.productSelected = product;
    this._modalProdServ.openModal();
  }
}
