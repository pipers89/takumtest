import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  modalShow = false;

  constructor() { }

  public openModal() {
    this.modalShow = true;
  }

  public closeModal() {
    this.modalShow = false;
  }
}