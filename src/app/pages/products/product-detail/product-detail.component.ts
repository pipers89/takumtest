import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/models/products.model';
import { ModalService } from './productModal.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  @Input() product: any;
  productDetail: Product[] = [];
  nameProdDetail: string = '';
  tittleDetail: string = 'Detalle del producto: ';
  constructor(
    private _modalProdServ: ModalService
  ) { }

  ngOnInit() {
    this.nameProdDetail = this.product.name;
    this.productDetail = this.product.products;
  }

  closeModal() {
    this._modalProdServ.closeModal();
  }

}
