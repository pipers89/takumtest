import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Import customs routes
import { PAGES_ROUTES } from './pages.routing';

import { ProductsComponent } from './products/products.component';
import { ProductDetailComponent } from './products/product-detail/product-detail.component';

@NgModule({
  declarations: [
    ProductsComponent,
    ProductDetailComponent,
  ],
  exports: [
    ProductsComponent,
    ProductDetailComponent
  ],
  imports: [
    CommonModule,
    PAGES_ROUTES
  ]
})
export class PagesModule { }
