import { RouterModule, Routes } from '@angular/router';

//Import Components
import { ProductsComponent } from './products/products.component';

//Imports Guards
import { AuthGuard } from '../guards/auth.guard';


const pagesRoutes: Routes = [
    { path: '', redirectTo: 'products', pathMatch: 'full' },
    { path: 'products', component: ProductsComponent, canActivateChild: [AuthGuard] }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);