import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';

//Services
import { AuthService } from '../services/auth.service';

import swal from 'sweetalert2';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User } from '../models/user.model';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    user: User;

    constructor(
        private _router: Router
    ) {
        if (localStorage.getItem('USER')) {
            this.user = JSON.parse(localStorage.getItem('USER')) as User;
        }
    }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(err => {
                if (err.status === 401) {
                    swal.fire('No Autorizado', `Usuario: ${this.user.username}, No esta autorizado`, 'warning');
                    this._router.navigate(['/login']);
                }

                if (err.status === 403) {
                    swal.fire('Acceso denegado', `Usuario: ${this.user.username}, No tienes acceso a este recurso`, 'warning');
                    this._router.navigate(['/pages/products']);
                }

                return throwError(err);
            })
        );
    }
}