import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../..//models/user.model';

import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  tittleLogin: string = 'Sing In';
  user: User = new User();

  constructor(
    private authServ: AuthService,
    private _router: Router
  ) { }

  ngOnInit() {
  }

  login() {
    if (this.user.username == null || this.user.password == null) {
      Swal.fire('Error Login', 'Username o password invalidos o vacios', 'error');
      return;
    }

    this.authServ.login(this.user)
    .subscribe(resp => {
      this.authServ.saveToken(resp.data.token);
      this.authServ.saveUser(resp.data);
      this._router.navigate(['/pages/products']);
      Swal.fire('Login', `Bienvenid@...!!! ${this.user.username}, has iniciado sessión exitosamente...`, 'success');
    }, err => {
      if (err.status === 400) {
        this.user.username = '';
        this.user.password = '';
        Swal.fire('Error Login', 'Username o password incorrectas...!!!', 'error');
        return;
      }
    });
  }

}
