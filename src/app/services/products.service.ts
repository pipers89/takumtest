import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

//Import config variables app
import { CONFIG } from '../config/config';

//Import Models
import { Product } from '../models/products.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public URL_SERVICES: string = CONFIG.URL_SERVICES;
    private _httpHeaders = new HttpHeaders(
        {
            'Content-Type': 'application/json'
        }
    );

  constructor(
    private router: Router,
    private http: HttpClient
  ) { }

  public getProducts(): Observable<any> {
    return this.http.get(`${this.URL_SERVICES}products`, { headers: this._httpHeaders })
    .pipe(resp => resp);
  }
}
