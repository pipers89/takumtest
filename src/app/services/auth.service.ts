import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//Import Models
import { User } from '../models/user.model';

//Config
import { CONFIG } from '../config/config';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    // CREDENTIALS: btoa('angularapp'+ ':' +'@_Aft081889') --- 'Authorization': 'Basic ' + this.credentials
    public URL_SERVICES: string = CONFIG.URL_SERVICES;
    private credentials = CONFIG.CREDENTIALS;
    private _httpHeaders = new HttpHeaders(
        {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + this.credentials
        }
    );
    private params = new URLSearchParams();
    private _token: string;
    private _user: User;

    constructor(

        private http: HttpClient
    ) { }

    public get user(): User {
        if (localStorage.getItem('USER')) {
            this._user = JSON.parse(localStorage.getItem('USER')) as User;
            return this._user;
        }

        return new User();
    }

    public get token(): string {
        if (localStorage.getItem('TOKEN') !== null) {
            this._token = localStorage.getItem('TOKEN');
            return this._token;
        }

        return null;
    }

    login(user: User) {
        this.params.set('username', user.username);
        this.params.set('password', user.password);
        let url = this.URL_SERVICES + 'users';
        return this.http.post<any>(`${url}`, this.params.toString(), { headers: this._httpHeaders });
    }

    saveToken(token: string) {
        this._token = (token !== null) ? token : null;
        localStorage.setItem('TOKEN', this._token);
    }

    saveUser(data: any): void {
        if (data) {
            this._user = new User();
            this._user.username = data.username;
            this._user.lastname = data.lastname;

            localStorage.setItem('DATA', JSON.stringify(this._user));
        }
    }

    isAuthenticated(): boolean {
        let token = (localStorage.getItem('TOKEN') !== null) ? localStorage.getItem('TOKEN') : null;

        if (token) {
            return true;
        }

        return false;
    }

    logout(): void {
        this._token = null;
        localStorage.clear();
    }
}